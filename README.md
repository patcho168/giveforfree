# Give For Free

**Group 3**

Application Name: Give For Free

URL: https://www.giveforfree.sg/

Visit our website [here](https://www.giveforfree.sg/)!

| Name                   | Matric No. | Contribution                                                                                             |
|------------------------|------------|----------------------------------------------------------------------------------------------------------|
| Huang Lie Jun          | A0123994W  | All the front end design.                                                                                |
| Ken Oung Yong Quan     | A0139388M  | Backend for gift creation and storage of image data, input validation, XSS and CSRF attack filtering     |
| Ng Xu Jie              | A0127363H  | Market research, conducting user validation and testing. Documentation of non-technical related matters. |
| Patrick Cho Chung Ting | A0065571A  | Backend API Calls, Integration with front-end, SQL Query Generation                                      |